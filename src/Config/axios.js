// First we need to import axios.js
import settings from './settings';
import AsyncStorage from '@react-native-community/async-storage';
import * as request from 'axios';
// Next we make an 'instance3' of it
const axios = request.create({
  // .. where we make our configurations
  baseURL: settings.API_URL,
  timeout: 20000,
});

axios.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      config.headers.Authorization = 'Bearer ' + token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

const get = async (url, data) => {
  try {
    const result = await axios(url, {
      method: 'GET',
      params: data,
      headers: {
        Accept: 'application/json',
      },
    });
    return result;
  } catch (error) {
    return Promise.reject(error);
  }
};

const post = async (url, data) => {
  try {
    console.log(url, data, 'dataa');
    const result = await axios(url, {
      method: 'POST',
      data: data,
      headers: {
        Accept: 'application/json',
      },
    });
    return result;
  } catch (error) {
    return Promise.reject(error);
  }
};

export default {get, post};
