import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const Home = () => {
  const [text, setText] = useState<string>('0');
  const [calcs, setCalcs] = useState<string>('');
  const calc = (texts: string, type: string) => {
    let current = +text;
    if (calcs !== texts) {
      switch (type) {
        case '+': {
          setCalcs(texts);
        }
        case '-': {
          setCalcs('');
        }
        case '=': {
          let set = current;
          setText(set + '');
          setCalcs('');
          break;
        }
        case 'clear': {
          setText('0');
          setCalcs('');
          break;
        }
        default: {
          setCalcs('');
        }
      }
    }
  };
  return (
    <SafeAreaView>
      <View>
        <TextInput
          keyboardType="numeric"
          editable={false}
          selectTextOnFocus={false}
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => console.log(text)}
          value={text}
        />
      </View>
      <View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#F25f65',
              marginHorizontal: 10,
              flex: 2,
            }}>
            <TouchableOpacity onPress={() => calc('0', '0')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>0</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('0', 'clear')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>
                clear
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('0', '=')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>
                {' '}
                ={' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#F25f65',
              marginHorizontal: 10,
              flex: 2,
            }}>
            <TouchableOpacity onPress={() => calc('1', '1')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>1</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('2', '2')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>2</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('3', '3')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>3</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#F25f65',
              marginHorizontal: 10,
              flex: 2,
            }}>
            <TouchableOpacity onPress={() => calc('4', '4')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>4</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('5', '5')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>5</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('6', '6')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>6</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#F25f65',
              marginHorizontal: 10,
              flex: 2,
            }}>
            <TouchableOpacity onPress={() => calc('7', '7')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>7</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('8', '8')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>8</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('9', '9')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>9</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#F25f65',
              marginHorizontal: 10,
              flex: 2,
            }}>
            <TouchableOpacity onPress={() => calc('', '-')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>-</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F25f65',
              paddingHorizontal: 10,
            }}>
            <TouchableOpacity onPress={() => calc('', '+')}>
              <Text style={{paddingHorizontal: 5, paddingVertical: 5}}>+</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Home;
